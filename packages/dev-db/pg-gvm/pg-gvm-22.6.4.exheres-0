# Copyright 2022-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=greenbone tag=v${PV} ] cmake
require postgresql [ multiunpack=false ]

SUMMARY="PostgreSQL extension for GVMd functions"
LICENCES="GPL-3"
PLATFORMS="~amd64"
SLOT="0"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.42]
        net-analyzer/gvm-libs[>=22.6]
        office-libs/libical:=[>=1.00]
"

configure_one_multibuild() {
    local cmakeparams=(
        -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
        -DDEBUG_FUNCTION_NAMES:BOOL=FALSE
        -DENABLE_COVERAGE:BOOL=FALSE
        -DPGCONFIG:PATH=/usr/$(exhost --target)/libexec/postgresql-${MULTIBUILD_TARGET}/pg_config
        -DPostgreSQL_TYPE_INCLUDE_DIR:PATH=/usr/$(exhost --target)/include/postgresql-${MULTIBUILD_TARGET}/server
    )

    ecmake "${cmakeparams[@]}"
}

install_one_multibuild() {
    cmake_src_install
}

