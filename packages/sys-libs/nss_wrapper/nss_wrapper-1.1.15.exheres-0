# Copyright 2015-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

SUMMARY="A wrapper for the user, group and hosts NSS API"
DESCRIPTION="
* Provides information for user and group accounts.
* Network name resolution using a hosts file.
* Loading and testing of NSS modules.

There are projects that need to be able to create, modify, and delete Unix users. Others just
switch user IDs to interact with the system on behalf of another user (e.g. a user space file
server). To be able to test applications like these, you need to grant privileges to modify the
passwd and group files. With nss_wrapper it is possible to define your own passwd and group files
to be used the software while it is under test. The nss_wrapper also allows you to create a hosts
file to set up name resolution for the addresses you use with socket_wrapper.
"
HOMEPAGE="https://cwrap.org/${PN}.html"
DOWNLOADS="mirror://samba/../cwrap/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    run:
        dev-lang/perl:* [[ note = [ nss_wrapper.pl ] ]]
    test:
        dev-util/cmocka[>=1.1.0]
        sys-libs/uid_wrapper[>=1.3.0]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-cmake-Fix-cmocka-1.1.6-find_package-in-CONFIG-mode.patch
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DUNIT_TESTING:BOOL=TRUE DUNIT_TESTING:BOOL=FALSE'
)

