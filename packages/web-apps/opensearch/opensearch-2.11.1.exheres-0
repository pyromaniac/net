# Copyright 2021-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Open source distributed and RESTful search engine"
HOMEPAGE="https://opensearch.org/"
DOWNLOADS="https://artifacts.opensearch.org/releases/bundle/${PN}/${PV}/${PNV}-linux-x64.tar.gz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    security [[ description = [ Enable security features which is strongly encouraged ] ]]
"

RESTRICT="strip"

DEPENDENCIES="
    build+run:
        group/opensearch
        user/opensearch
"

src_install() {
    insinto /etc/default
    doins "${FILES}"/opensearch

    insinto /etc/opensearch
    doins -r config/*

    insinto /usr/share/opensearch
    doins -r {bin,jdk,lib,modules,plugins}
    doins {NOTICE.txt,README.md}
    insinto /usr/share/opensearch/bin
    doins "${FILES}"/systemd-entrypoint

    keepdir /etc/opensearch/jvm.options.d
    keepdir /var/{lib,log}/opensearch
    keepdir /usr/share/opensearch/extensions

    edo chmod 0755 "${IMAGE}"/usr/share/opensearch/{jdk/,}bin/*
    edo chmod 0755 "${IMAGE}"/usr/share/opensearch/jdk/lib/{jexec,jspawnhelper}
    edo chmod 0755 "${IMAGE}"/usr/share/opensearch/plugins/opensearch-security/tools/*.sh
    edo chown -R opensearch:opensearch "${IMAGE}"/etc/opensearch
    edo chown opensearch:opensearch "${IMAGE}"/var/{lib,log}/opensearch

    if ! option security; then
        echo "plugins.security.disabled: true" >> "${IMAGE}"/etc/opensearch/opensearch.yml
    fi

    install_systemd_files

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins opensearch.conf <<EOF
d /run/opensearch 0755 opensearch opensearch - -
EOF

    insinto /usr/$(exhost --target)/lib/sysctl.d
    hereins opensearch.conf <<EOF
vm.max_map_count=262144
EOF

    # opensearch.yml
    edo sed \
        -e 's@#path.data: /path/to/data@path.data: /var/lib/opensearch@g' \
        -e 's@#path.logs: /path/to/logs@path.logs: /var/log/opensearch@g' \
        -i "${IMAGE}"/etc/opensearch/opensearch.yml

    # jvm.options
    edo sed \
        -e 's:HeapDumpPath=data:HeapDumpPath=/var/lib/opensearch:g' \
        -e 's:ErrorFile=logs:ErrorFile=/var/log/opensearch:g' \
        -e 's:file=logs:file=/var/log/opensearch:g' \
        -i "${IMAGE}"/etc/opensearch/jvm.options
}

pkg_postinst() {
    elog "To configure OpenSearch Security consult the documentation:"
    elog "https://opensearch.org/docs/security-plugin/configuration/index/"
    elog "Alternatively setup a working demo configuration which is *NOT*"
    elog "suited for production use or public reachable systems by running:"
    elog "/usr/share/opensearch/plugins/opensearch-security/tools/install_demo_configuration.sh"
}

