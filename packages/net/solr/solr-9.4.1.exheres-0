# Copyright 2020-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Popular, blazing-fast, open source enterprise search platform built on Apache Lucene"
DESCRIPTION="
Solr is highly reliable, scalable and fault tolerant, providing distributed indexing, replication
and load-balanced querying, automated failover and recovery, centralized configuration and more.
Solr powers the search and navigation features of many of the world's largest internet sites.
"
HOMEPAGE="https://${PN}.apache.org"
DOWNLOADS="https://archive.apache.org/dist/${PN}/${PN}/${PV}/${PNV}.tgz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/resources.html [[ lang = en ]]"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="strip"

DEPENDENCIES="
    build+run:
        group/${PN}
        user/${PN}
    run:
        virtual/jre:*[>=11.0]
"

pkg_setup() {
    exdirectory --allow /opt
}

src_test() {
    :
}

src_prepare() {
    edo sed \
        -e 's:#SOLR_PID_DIR=:SOLR_PID_DIR=/run/solr:g' \
        -e 's:#SOLR_HOME=:SOLR_HOME=/var/lib/solr/data:g' \
        -e 's:#LOG4J_PROPS=/var/solr/log4j2.xml:LOG4J_PROPS=/var/lib/solr/log4j2.xml:g' \
        -e 's:#SOLR_LOGS_DIR=logs:SOLR_LOGS_DIR=/var/log/solr:g' \
        -i bin/solr.in.sh

    default
}

src_install() {
    insinto /etc/default
    doins bin/solr.in.sh

    insinto /var/lib/solr
    doins server/resources/log4j2.xml

    insinto /var/lib/solr/data
    doins server/solr/{README.md,solr.xml,zoo.cfg}

    insinto /opt/${PN}
    doins -r *

    edo rm -rf "${IMAGE}"/opt/${PN}/bin/{init.d,install_solr_service.sh,solr.in.sh}
    edo chmod -R 0755 "${IMAGE}"/opt/${PN}/bin/*

    keepdir /var/log/${PN}
    edo chown -R solr:solr "${IMAGE}"/var/{lib,log}/${PN}

    install_systemd_files

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/solr 0755 solr solr
EOF
}

